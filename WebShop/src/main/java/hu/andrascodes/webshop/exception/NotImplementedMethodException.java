/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.exception;

/**
 *
 * @author andra
 */
public class NotImplementedMethodException extends Exception {

    public NotImplementedMethodException(String... message) {
        super("This method is not implemented." + message);
    }

}
