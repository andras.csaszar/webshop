/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author andra
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Entity
@Table(name="product_category")
public class ProductCategory implements Serializable{
    @EqualsAndHashCode.Include
    @Id
    @Column(name="product_category_id")
    private Integer id;
    @Column(name="product_category_name")
    private String name;
    @Column(name="is_active")
    private boolean isActive;
    @OneToMany(mappedBy= "productCategory", cascade = CascadeType.ALL)
    private List<ProductSubCategory> subcategories;
}
