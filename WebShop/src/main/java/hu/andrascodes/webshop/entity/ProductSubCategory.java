/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author andra
 */

@Getter
@Setter
@EqualsAndHashCode (onlyExplicitlyIncluded = true)
@ToString
@Entity
@Table(name="product_subcategory")
public class ProductSubCategory implements Serializable {
    @EqualsAndHashCode.Include
    @Id
    @Column(name="product_subcategory_id")
    private Integer id;
    @Column(name="product_subcategory_name")
    private String name;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="product_category_id")
    private ProductCategory productCategory;
    @Column(name="is_active")
    private boolean ative;
}
