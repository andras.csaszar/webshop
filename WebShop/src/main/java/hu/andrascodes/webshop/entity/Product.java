/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author andra
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Entity
@Table(name="product")
public class Product implements Serializable{
    @EqualsAndHashCode.Include
    @Id
    @Column(name="product_id")
    private Integer id;
    @Column(name="product_name")
    private String name;
    @ManyToOne
    private Brand brand;
    @Column(name="short_description")
    private String shortDescription;
    @Column(name="full_description")
    private String fullDescription;
    @Column(name="product_quantity")
    private int quantity;
    @Column(name="product_price")
    private int price;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="product_subcategory_id")
    private ProductSubCategory subcategory;
    @Column(name="product_image_path")
    private String productImagePath;
    @Column(name="is_active")
    private boolean active;
    
}
