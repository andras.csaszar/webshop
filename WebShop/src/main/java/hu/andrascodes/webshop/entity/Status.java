/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author andra
 */
@Getter
@Setter
@EqualsAndHashCode (onlyExplicitlyIncluded = true)
@ToString
@Entity
@Table(name="status")
public class Status implements Serializable{
    @EqualsAndHashCode.Include
    @Id
    @Column(name="status_id")
    private Integer id;
    @Column(name="status_name")
    private String name;
    
}
