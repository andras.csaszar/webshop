/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.entity.Product;
import hu.andrascodes.webshop.repository.repo.CrudRepository;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class ProductDao implements CrudRepository<Product, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(Product product) throws Exception {
        em.persist(product);
        return true;
    }

    @Override
    public List<Product> findAll() throws Exception {
        return em.createQuery("SELECT p FROM Product p", Product.class)
                .getResultList();
    }

    @Override
    public Optional<Product> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("SELECT p FROM Product p WHERE p.id= :id", Product.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<Product>> findByName(String name) throws Exception {
        return Optional.of(
                em.createQuery("SELECT p FROM Product p WHERE p.name LIKE :name", Product.class)
                        .setParameter("name", "%" + name + "%")
                        .getResultList()
        );
    }

    @Override
    public boolean update(Product product) throws Exception {
        return Optional.ofNullable(
                em.merge(product)
        ).isPresent();

    }

    @Override
    public boolean delete(Product product) throws Exception {
        em.remove(product);
        return true;

    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT(p) FROM Product p").getSingleResult();
    }

}
