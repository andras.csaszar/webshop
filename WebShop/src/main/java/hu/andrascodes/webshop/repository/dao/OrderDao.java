/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.repository.repo.CrudRepository;
import hu.andrascodes.webshop.entity.Order;
import hu.andrascodes.webshop.exception.NotImplementedMethodException;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class OrderDao implements CrudRepository<Order, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(Order order) throws Exception {
        em.persist(order);
        return true;
    }

    @Override
    public List<Order> findAll() throws Exception {
        return em.createQuery("SELECT o FROM Order o", Order.class)
                .getResultList();
    }

    @Override
    public Optional<Order> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("Select o FROM Order o WHERE o.id=: id", Order.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );

    }

    @Override
    public Optional<Iterable<Order>> findByName(String name) throws Exception {
        throw new NotImplementedMethodException("No name attribute of Order");
    }

    @Override
    public boolean update(Order order) throws Exception {
        return Optional.ofNullable(em.merge(order)).isPresent();

    }

    @Override
    public boolean delete(Order order) throws Exception {
        em.remove(order);
        return true;
    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT (o) FROM Order o")
                .getSingleResult();
    }

}
