/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.entity.ProductCategory;
import hu.andrascodes.webshop.repository.repo.CrudRepository;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class ProductCategoryDao implements CrudRepository<ProductCategory, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(ProductCategory productCategory) throws Exception {
        em.persist(productCategory);
        return true;
    }

    @Override
    public List<ProductCategory> findAll() throws Exception {
        return em.createQuery("SELECT pc FROM ProductCategory pc", ProductCategory.class)
                .getResultList();
    }

    @Override
    public Optional<ProductCategory> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("SELECT pc FROM ProductCategory pc WHERE pc.id= :id", ProductCategory.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<ProductCategory>> findByName(String name) throws Exception {
        return Optional.of(
                em.createQuery("SELECT pc FROM ProductCategory pc WHERE pc.name LIKE :name", ProductCategory.class)
                        .setParameter("name", "%" + name + "%")
                        .getResultList()
        );
    }

    @Override
    public boolean update(ProductCategory productCategory) throws Exception {
        return Optional.ofNullable(em.merge(productCategory)).isPresent();
    }

    @Override
    public boolean delete(ProductCategory productCategory) throws Exception {
        em.remove(productCategory);
        return true;
    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT(pc) FROM ProductCategory pc")
                .getSingleResult();
    }

}
