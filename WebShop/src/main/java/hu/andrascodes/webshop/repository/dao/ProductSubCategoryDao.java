/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.entity.ProductSubCategory;
import hu.andrascodes.webshop.repository.repo.CrudRepository;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class ProductSubCategoryDao implements CrudRepository<ProductSubCategory, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(ProductSubCategory productSubCategory) throws Exception {
        em.persist(productSubCategory);
        return true;
    }

    @Override
    public List<ProductSubCategory> findAll() throws Exception {
        return em.createQuery("SELECT psc FROM ProductSubCategory psc", ProductSubCategory.class)
                .getResultList();
    }

    @Override
    public Optional<ProductSubCategory> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("SELECT psc FROM ProductSubCategory psc WHERE psc.id=:id", ProductSubCategory.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<ProductSubCategory>> findByName(String name) throws Exception {
        return Optional.of(
                em.createQuery("SELECT psc FROM ProductSubCategory psc WHERE psc.name LIKE:name")
                        .setParameter("name", "%" + name + "%")
                        .getResultList()
        );

    }

    @Override
    public boolean update(ProductSubCategory productSubCategory) throws Exception {
        return Optional.ofNullable(em.merge(productSubCategory)).isPresent();
    }

    @Override
    public boolean delete(ProductSubCategory productSubCategory) throws Exception {
        em.remove(productSubCategory);
        return true;
    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT(psc) FROM ProductSubCategory psc")
                .getSingleResult();
    }

}
