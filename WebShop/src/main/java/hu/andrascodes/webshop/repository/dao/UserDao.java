/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.entity.User;
import hu.andrascodes.webshop.repository.repo.CrudRepository;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class UserDao implements CrudRepository<User, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(User user) throws Exception {
        em.persist(user);
        return true;
    }

    @Override
    public List<User> findAll() throws Exception {
        return em.createQuery("SELECT u FROM USER u", User.class)
                .getResultList();
    }

    /**
     * Finds user by unique identification attribute.
     *
     * @param id for User entity the unique ID is the username.
     */
    @Override
    public Optional<User> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("SELECT u FROM User u WHERE u.username=:username", User.class)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<User>> findByName(String name) throws Exception {
        return Optional.of(
                em.createQuery("SELECT u FROM User u WHERE u.username LIKE: username", User.class)
                        .setParameter("username", "%" + name + "%")
                        .getResultList()
        );
    }

    @Override
    public boolean update(User user) throws Exception {
        return Optional.ofNullable(em.merge(user)).isPresent();
    }

    @Override
    public boolean delete(User user) throws Exception {
        em.remove(user);
        return true;
    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT(u) FROM User u")
                .getSingleResult();
    }

}
