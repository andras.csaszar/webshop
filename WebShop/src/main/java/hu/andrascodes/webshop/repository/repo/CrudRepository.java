/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.repo;

import java.util.Optional;

/**
 *
 * @author andra
 */
public interface CrudRepository<Entity, Id> {
    
    boolean create(Entity entity) throws Exception;
    
    Iterable<Entity> findAll() throws Exception;

    Optional<Entity> findById(Id id) throws Exception;

    Optional<Iterable<Entity>> findByName(String name) throws Exception;

    boolean update(Entity entity) throws Exception;

    boolean delete(Entity entity) throws Exception;

    int countAll() throws Exception;

}
