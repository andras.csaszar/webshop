/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.repo;

/**
 *
 * @author andra
 */
public interface AttributeReadRepo<Entity> {

    Iterable<Entity> findByProductName(String productName);
    Iterable<Entity> findByUsername(String username);
    
}
