/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.entity.Status;
import hu.andrascodes.webshop.repository.repo.CrudRepository;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class StatusDao implements CrudRepository<Status, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(Status status) throws Exception {
        em.persist(status);
        return true;
    }

    @Override
    public List<Status> findAll() throws Exception {
        return em.createQuery("SELECT s FROM Status s", Status.class)
                .getResultList();

    }

    @Override
    public Optional<Status> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("SELECT s FROM Status s WHERE s.id=:id", Status.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<Status>> findByName(String name) throws Exception {

        return Optional.of(
                em.createQuery("SELECT s FROM Status s WHERE s.name LIKE :name", Status.class)
                        .setParameter("name", "%" + name + "%")
                        .getResultList()
        );
    }

    @Override
    public boolean update(Status status) throws Exception {
        return Optional.ofNullable(
                em.merge(status))
                .isPresent();
    }

    @Override
    public boolean delete(Status status) throws Exception {
        em.remove(status);
        return true;
    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT (s) FROM Status s")
                .getSingleResult();
    }

}
