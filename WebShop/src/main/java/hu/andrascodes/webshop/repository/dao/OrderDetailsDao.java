/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.repository.repo.CrudRepository;
import hu.andrascodes.webshop.entity.OrderDetails;
import hu.andrascodes.webshop.exception.NotImplementedMethodException;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class OrderDetailsDao implements CrudRepository<OrderDetails, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(OrderDetails orderDetails) throws Exception {
        em.persist(orderDetails);
        return true; //if does not throw exception before, this line is reached, so is true
    }

    @Override
    public List<OrderDetails> findAll() throws Exception {
        return em.createQuery("SELECT od FROM OrderDetails od", OrderDetails.class).getResultList();
    }

    @Override
    public Optional<OrderDetails> findById(Integer id) throws Exception {
        return Optional.of(
                em.createQuery("SELECT od FROM OrderDetails od WHERE id= :id", OrderDetails.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<OrderDetails>> findByName(String productName) throws Exception {
        throw new NotImplementedMethodException("No name attribute exists of OrderDetail");
    }

    @Override
    public boolean update(OrderDetails orderDetails) throws Exception {
        return Optional.ofNullable(
                em.merge(orderDetails)
        ).isPresent();
    }

    @Override
    public boolean delete(OrderDetails orderDetails) throws Exception {
        em.remove(orderDetails);
        return true; //if does not throw exception before, this line is reached, so is true

    }

    @Override
    public int countAll() throws Exception {
        return (int) em.createQuery("SELECT COUNT(od) FROM OrderDetails od").getSingleResult();
    }

}
