/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.webshop.repository.dao;

import hu.andrascodes.webshop.repository.repo.CrudRepository;
import hu.andrascodes.webshop.entity.Brand;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class BrandDao implements CrudRepository<Brand, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(Brand brand) {
        em.persist(brand);
        return true; //if does not throw exception before, this line is reached, so is true

    }

    @Override
    public List<Brand> findAll() {
        return em.createQuery("SELECT b FROM Brand b", Brand.class)
                .getResultList();
    }

    @Override
    public Optional<Brand> findById(Integer id) {
        //todo: try-catch, as this throws RuntimeException child
        return Optional.of(
                em.createQuery("SELECT b FROM Brand b WHERE b.id= :id", Brand.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    @Override
    public Optional<Iterable<Brand>> findByName(String name) {
        //todo: try-catch, as this throws RuntimeException child
        return Optional.of(
                em.createQuery("SELECT b FROM Brand b WHERE b.name LIKE :name", Brand.class)
                        .setParameter("name", "%" + name + "%")
                        .getResultList()
        );

    }

    @Override
    public boolean update(Brand brand) {
        return Optional.ofNullable(em.merge(brand)).isPresent();
    }

    @Override
    public boolean delete(Brand brand) {
        em.remove(brand);
        return true; //if does not throw exception before, this line is reached, so is true
    }

    @Override
    public int countAll() {
        return (int) em.createQuery("SELECT COUNT(b) FROM Brand b")
                .getSingleResult();
    }

}
